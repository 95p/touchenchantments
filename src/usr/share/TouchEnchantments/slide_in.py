#!/usr/bin/env python3

from rightclick import IsRightClick

RIGHT = 0
LEFT = 1
TOP = 2
BOTTOM = 3


def CheckForSlide(history_array, _size, variance, slide_side):
    data_amount = int(_size / 2)
    time = data_amount / 30
    variance /= 4
    correctDirection = False

    # Check if there's enough data
    if data_amount < 4:
        return False

    # Check if there's too much data
    if int(len(history_array)) < _size:
        return False

    # If the slide is coming from one side, check if it is made from the side
    if slide_side == RIGHT:
        # print(history_array[0])
        if history_array[0] > 0.1:
            return False

    if slide_side == LEFT:
        if history_array[0] < 0.9:
            return False

    # Get the way the cursor went
    for i in range(int(data_amount) - 1):
        movementX = history_array[i * 2 + 2] - history_array[i * 2]
        movementY = history_array[i * 2 + 3] - history_array[i * 2 + 1]

        # Right: Moved more to the right than up or down
        if slide_side == RIGHT:
            # Check if moved enough for a real slide, 5% of the screen dimension in which to move
            if movementX >= abs(movementY) and history_array[0]<0.05:
                correctDirection = True
                print("[info] Left Side, moving to the right")

        # Left: Moved more to the left than up or down
        elif slide_side == LEFT:
            if movementX <= - abs(movementY) and history_array[0]>0.95:
                print("[info] Right Side, moving to the left")
                correctDirection = True

        # Top: Moved more upwards than left or right
        elif slide_side == TOP:
            if movementY >= abs(movementX) and history_array[1]<0.05:
                print("[info] Top, moving down")
                correctDirection = True

        # The Slide to the right side means that went more negative x than y
        elif slide_side == BOTTOM:
            if movementY <= - abs(movementX) and history_array[1]>0.95:
                correctDirection = True
                print("[info] Bottom, moving up")
        else:
            print("FATAL")
    # Calculate the overall way to be gone
    movementX = history_array[data_amount - 2] - history_array[0]
    movementY = history_array[data_amount - 1] - history_array[1]

    if correctDirection:
        if slide_side == RIGHT or slide_side == LEFT:
            if abs(movementX) < variance:
                return False
        elif slide_side == TOP or slide_side == BOTTOM:
            if abs(movementY) < variance:
                return False

        return True
    else:
        return False


# Function to runtouch check if sliding
def Slide(_array, _size, _variance, _timeout):
    data_amount = int(_size / 2)
    time = data_amount / 30

    # Check if minimum time is fulfilled and if it isn't a long right click
    if time > _timeout and not IsRightClick(_array, _size, time):
        print("CHECKING " + str(_size))
        # Checking for if it is an up slide
        if CheckForSlide(_array, _size, _variance, TOP):
            print("Sliding from the top")
            return True
        # Checking for if it is an down slide
        if CheckForSlide(_array, _size, _variance, BOTTOM):
            print("Sliding from the bottom")
            return True
        if CheckForSlide(_array, _size, _variance, RIGHT):
            print("Sliding from the right")
            return True
        if CheckForSlide(_array, _size, _variance, LEFT):
            print("Sliding from the left")
            return True

    return False
