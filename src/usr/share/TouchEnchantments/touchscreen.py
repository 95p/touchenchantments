#!/usr/bin/env python3

from evdev import InputDevice, ecodes, categorize

# Test
from time import sleep
from detectdevices import DetectTouchDevices


class Touchscreen(object):
    def __init__(self, ev_path):
        self.dev = InputDevice(ev_path)
        self.points = {}
        self.slot = 0

        caps = self.dev.capabilities()
        self.max_x = caps[ecodes.EV_ABS][ecodes.ABS_X][1].max
        self.max_y = caps[ecodes.EV_ABS][ecodes.ABS_Y][1].max

    def update(self):
        while 1:
            ev = self.dev.read_one()
            if not ev:
                break
            if ev.type == ecodes.EV_ABS:
                if ev.code == ecodes.ABS_MT_TRACKING_ID:
                    if ev.value != -1:
                        self.points[self.slot] = [0., 0.]
                    elif self.slot in self.points: # ignore invalid
                        del self.points[self.slot]
                elif ev.code == ecodes.ABS_MT_SLOT:
                    self.slot = ev.value
                elif self.slot in self.points:
                    if ev.code in (ecodes.ABS_X, ecodes.ABS_MT_POSITION_X):
                        self.points[self.slot][0] = ev.value / self.max_x
                    elif ev.code in (ecodes.ABS_Y, ecodes.ABS_MT_POSITION_Y):
                        self.points[self.slot][1] = ev.value / self.max_y

#scrs = DetectTouchDevices()['screens']
#if len(scrs) > 0:
#    screen = Touchscreen(scrs[0])
#    while 1:
#        screen.update()
#        if 0 in screen.points:
#            print(screen.points[0])
#        sleep(1/30)
#
