#!/usr/bin/env python3

# A function that checks if two positions differ by more than v = 5 units
def IsNotTooFarAway(x1, y1, x2, y2, v = 0.01):
    if abs(x1 - x2) >= v:
        return False
    if abs(y1 - y2) >= v:
        return False
    return True


# noinspection PySimplifyBooleanCheck
def IsRightClick(_array, _size, _timeout):
# Calculate the time needed for the gesture
    data_amount = _size / 2
    time = data_amount / 30

    #print("time = " +str( time))
    if time < _timeout:
        return False

# check for every point if it is too far away from its previous point or too far away from the first point
    for i in range(int(data_amount) - 1):
        # Check if the last point isn't too far away from previous point
        if IsNotTooFarAway(_array[i*2], _array[i*2 + 1], _array[i*2 + 2], _array[i*2 + 3], 0.03) == False:
            #print(1)
            return False
        # Check if the first point isn't too far away from the current point
        if not IsNotTooFarAway(_array[0], _array[1], _array[i * 2], _array[i * 2 + 1], 0.01):
            #print(2)
            return False

    return True


#pygame.init()
#
#print(str(pygame.mouse.get_pressed()))
#
#pygame.event.get()
#
#while pygame.mouse.get_pressed()[0] == False:
#    print(pygame.mouse.get_pressed()[0])
#    time.sleep(1)
#
#
#while pygame.mouse.get_pressed()[0] == True:
#    _x = "x" in pygame.mouse.get_pos()
#    _y = "y" in pygame.mouse.get_pos()
#    print("_x = " + str(_x) + ", _y = " + str(_y))
#
#testarray = [0, 0, 4, 0, 0, 2, 0, 6, 0, 9, 0, 12, 0, 16, 0 ,20, 0, 24, 0, 0, 0, 0]
#v = IsRightclick(testarray)
#print("v = " + str(v))
