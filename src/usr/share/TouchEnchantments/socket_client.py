#!/usr/bin/env python3
import socket
import os
from pathlib import Path
import readconfig
import subprocess
from time import sleep

home = str(Path.home())

MASTER_SOCKET_PATH="/run/touchenchantments.socket"
LOCAL_SOCKET_PATH=home+"/.local/touchenchantments.socket"

config_file = readconfig.TouchConfigReader()

try:
    while True:
        sleep(0.5)
        # clear path
        if os.path.exists(LOCAL_SOCKET_PATH):
            os.remove(LOCAL_SOCKET_PATH)

        # Creating local socket

        server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server.bind(LOCAL_SOCKET_PATH)
        server.listen(1)

        print("Connecting...")

        try:
            master = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            master.connect(MASTER_SOCKET_PATH)
            master.send(LOCAL_SOCKET_PATH.encode('utf-8'))
            print("Sent local socket to master.")
            print("Ctrl-C to quit.")

            print("Listening...")
            conn, addr = server.accept()
        except:
            print("Failed to connect to master service")

            continue
        print("Connected to master service")
        while True:
            datagram = conn.recv(1024)
            if not datagram:
                continue
            else:
                cmd=datagram.decode('utf-8')
                if cmd=="SLIDELEFT":
                    subprocess.run(config_file.getConfig("SwipeLeft", "Command").split(" "))
                elif cmd=="SLIDERIGHT":
                    subprocess.run(config_file.getConfig("SwipeRight", "Command").split(" "))
                elif cmd=="SLIDEUP":
                    subprocess.run(config_file.getConfig("SwipeUp", "Command").split(" "))
                elif cmd=="SLIDEDOWN":
                    subprocess.run(config_file.getConfig("SwipeDown", "Command").split(" "))

except KeyboardInterrupt as k:
    print("Shutting down.")
    server.close()