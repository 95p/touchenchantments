#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
rm -rf /usr/share/TouchEnchantments/ /etc/TouchEnchantments/ /etc/systemd/system/touchenchantments.service
cp -r src/usr/share/TouchEnchantments/ /usr/share/
cp -r src/etc/* /etc/
chown -R root /usr/share/TouchEnchantments/ /etc/TouchEnchantements/ /etc/systemd/system/touchenchantments.service
systemctl enable /etc/systemd/system/touchenchantments.service

systemctl start touchenchantments.service
